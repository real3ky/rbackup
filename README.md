# rbackup

Bash script to backup files on a device, regularly, with rsync.
Detect if the drive is mounted and notify with a dialog if it's not.
